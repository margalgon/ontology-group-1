package es.upm.emse.absd.ontology;

import es.upm.emse.absd.ontology.AgentActions.ChangePhase;
import es.upm.emse.absd.ontology.AgentActions.InformInitialResources;
import es.upm.emse.absd.ontology.AgentActions.Register;
import es.upm.emse.absd.ontology.Concepts.NewPhase;
import es.upm.emse.absd.ontology.Concepts.Resource;
import es.upm.emse.absd.ontology.Concepts.Tribe;
import es.upm.emse.absd.ontology.Concepts.TribeResources;
import jade.content.onto.*;
import jade.content.schema.*;

/** file: WoAOntology.java
 * @version 2023/03/17
 */

public class WoAOntology extends jade.content.onto.Ontology {
    //NAME
    public static final String ONTOLOGY_NAME = "woaOntology";
    // The singleton instance of this ontology
    private static final ReflectiveIntrospector introspect = new ReflectiveIntrospector();
    private static final Ontology theInstance = new WoAOntology();
    public static Ontology getInstance() {
        return theInstance;
    }


    /** VOCABULARY
     * Feel free to open conversation about this rule to modify, expand or if it's wrong ;)
     * To improve understanding and readability of the ontology vocabulary we're adding
     * a framework to work and add new information following the rule of
     * adding the vocabulary in the corresponding block with its suffix:
     *  - Actions: *_AC
     *  - Actions Properties: *_PY
     *  - Concepts: *_CP
     *  - Concepts Properties: *_PY
     *  - Predicates: Any suggestions??
     */
    // Actions vocabulary
    public static final String REGISTER_AC = "Register";
    public static final String CHANGE_PHASE_AC = "ChangePhase";
    public static final String INFORM_INITIAL_RESOURCES_AC = "InformInitialResources";

    // Actions Properties vocabulary
    public static final String TRIBE_PY = "tribe";
    public static final String TRIBE_RESOURCES_PY = "tribeResources";

    // Concepts vocabulary
    public static final String TRIBE_CP = "Tribe";
    public static final String TRIBE_RESOURCES_CP = "TribeResources";
    public static final String RESOURCES_CP = "Resources";
    public static final String NEW_PHASE_CP = "NewPhase";

    // Concepts Properties vocabulary
    public static final String TRIBE_NAME_PY= "tribeName";
    public static final String WOOD_PY= "wood";

    public static final String GOLD_PY= "gold";

    public static final String STONE_PY= "stone";
    public static final String TYPE_RES_PY= "typeRes";
    public static final String AMOUNT_PY= "amount";
    public static final String PHASE_PY= "phase";

    // Predicates vocabulary

    /**
     * Constructor
     */
    private WoAOntology(){
        super(ONTOLOGY_NAME, BasicOntology.getInstance());
        try {
            // adding AgentAction(s)
            AgentActionSchema registerSchema = new AgentActionSchema(REGISTER_AC);
            add(registerSchema, Register.class);
            AgentActionSchema informInitialResourcesSchema = new AgentActionSchema(INFORM_INITIAL_RESOURCES_AC);
            add(informInitialResourcesSchema, InformInitialResources.class);
            AgentActionSchema changePhaseSchema = new AgentActionSchema(CHANGE_PHASE_AC);
            add(changePhaseSchema, ChangePhase.class);


            // adding Concept(s)
            ConceptSchema tribeSchema = new ConceptSchema(TRIBE_CP);
            add(tribeSchema, Tribe.class);
            ConceptSchema resourceSchema = new ConceptSchema(RESOURCES_CP);
            add(resourceSchema, Resource.class);
            ConceptSchema tribeResourcesSchema = new ConceptSchema(TRIBE_RESOURCES_CP);
            add(tribeResourcesSchema, TribeResources.class);
            ConceptSchema newPhaseSchema = new ConceptSchema(NEW_PHASE_CP);
            add(newPhaseSchema, NewPhase.class);

            // adding Predicate(s)


            // adding AgentAction(s)' fields
            registerSchema.add(TRIBE_PY, tribeSchema, ObjectSchema.MANDATORY);
            informInitialResourcesSchema.add(TRIBE_RESOURCES_PY, tribeResourcesSchema, ObjectSchema.MANDATORY);

            // adding Concept(s)' fields
            tribeSchema.add(TRIBE_NAME_PY, (TermSchema)getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
            resourceSchema.add(TYPE_RES_PY, (TermSchema)getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
            resourceSchema.add(AMOUNT_PY, (TermSchema)getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);
            tribeResourcesSchema.add(WOOD_PY, resourceSchema, ObjectSchema.MANDATORY);
            tribeResourcesSchema.add(GOLD_PY, resourceSchema, ObjectSchema.MANDATORY);
            tribeResourcesSchema.add(STONE_PY, resourceSchema, ObjectSchema.MANDATORY);
            newPhaseSchema.add(PHASE_PY, (TermSchema)getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);


            // adding Predicate(s)' fields


            // adding name mappings
            // adding inheritance

        }catch (java.lang.Exception e) {e.printStackTrace();}
    }
}
