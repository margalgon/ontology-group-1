package es.upm.emse.absd.ontology.AgentActions;

import es.upm.emse.absd.ontology.Concepts.Tribe;
import jade.content.*;
import lombok.Data;

/**
 * Protege name: Register
 *
 * @version 2023/03/17
 */
@Data
public class Register implements AgentAction {
    private Tribe tribe;
}
