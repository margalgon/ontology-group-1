package es.upm.emse.absd.ontology.AgentActions;

import es.upm.emse.absd.ontology.Concepts.TribeResources;
import jade.content.AgentAction;
import lombok.Data;

/**
 * Protege name: Register
 * @version 2023/03/18
 */
@Data
public class InformInitialResources implements AgentAction {
    private TribeResources tribeResources;
    private int initialStorage;
}
