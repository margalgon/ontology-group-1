package es.upm.emse.absd.ontology.Concepts;

import jade.content.*;
import lombok.Data;

/**
 * Protege name: Resource
 * @version 2023/03/18
 */
@Data
public class Resource implements Concept {
    final private ResourceType typeRes;

    private float amount;

    public Resource(){
        this.typeRes = ResourceType.WOOD;
    }
}
