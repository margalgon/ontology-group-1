package es.upm.emse.absd.ontology.Concepts;

import jade.content.*;
import jade.util.leap.*;
import jade.core.*;
import lombok.Data;

/**
 * Protege name: Tribe
 * @version 2023/03/17
 */
@Data
public class Tribe implements Concept {

    /**
     * Protege name: name
     */
    private String name;

}