package es.upm.emse.absd.ontology.Concepts;

public enum ResourceType {
    WOOD,
    STONE,
    GOLD
}
