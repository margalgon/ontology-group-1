package es.upm.emse.absd.ontology.Concepts;

import jade.content.Concept;
import lombok.Data;

/**
 * Protege name: TribeResources
 * @version 2023/03/18
 */
@Data
public class TribeResources implements Concept {
    private Resource wood;
    private Resource stone;
    private Resource gold;
}
