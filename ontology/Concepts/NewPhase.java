package es.upm.emse.absd.ontology.Concepts;

import jade.content.Concept;
import lombok.Data;

/**
 * Protege name: NewPhase
 * @version 2023/03/18
 */
@Data
public class NewPhase implements Concept {

    private String phaseName;

    private Integer duration;
}
